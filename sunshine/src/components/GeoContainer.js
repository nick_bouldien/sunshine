import React from "react";
import PropTypes from "prop-types";

class GeoContainer extends React.Component {
  static propTypes = {
    children: PropTypes.func.isRequired,
  };

  state = {
    coordinates: {
      lat: null,
      lng: null,
    },
    error: null,
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState(state => ({
          ...state,
          coordinates: {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          },
        }));
      },
      error => {
        this.setState({ error: "Could not access your location." });
      },
    );
  }

  render() {
    return this.props.children(this.state);
  }
}

export default GeoContainer;
