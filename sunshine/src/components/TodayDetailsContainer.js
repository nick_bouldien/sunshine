import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import Spinner from "./Spinner";
import TodayDetails from "./TodayDetails";
import { createCurrentWeatherUrl } from "../utils/urlCreator";

@inject("currentWeatherStore")
@observer
class TodayDetailsContainer extends Component {
  state = {};
  componentDidMount() {
    if (this.props.useLocation && (!this.props.lat || !this.props.lng)) {
      return;
    } else {
      this.fetchData();
    }
  }

  componentDidUpdate(prevProps) {
    const { lat, lng } = this.props;
    if (
      this.props.useLocation !== prevProps.useLocation ||
      lat !== prevProps.lat ||
      lng !== prevProps.lng
    ) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const { city, lat, lng, useLocation } = this.props;
    let url = "";
    if (useLocation && lat && lng) {
      url = createCurrentWeatherUrl(null, lat, lng);
    } else {
      url = createCurrentWeatherUrl(city);
    }
    this.props.currentWeatherStore.fetchCurrentWeather(url);
  };

  render() {
    const { error, isLoading, weather } = this.props.currentWeatherStore;
    if (error) {
      return <code>error: {error.toString()}</code>;
    }
    if (isLoading) {
      return <Spinner />;
    }
    return weather && <TodayDetails data={weather} />;
  }
}

export default TodayDetailsContainer;
