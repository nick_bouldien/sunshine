import React from "react";
import * as PropTypes from "prop-types";
import styled from "styled-components";
import SecondRow from "../styled_components/SecondRow";
import TopRow from "../styled_components/TopRow";

const DetailsSection = styled.div`
  background: linear-gradient(to right, #2e96ff, #28c2ff);
  color: #ffffff;
  font-family: Open Sans Regular;
  margin-bottom: 32px;
  padding: 16px;
`;

// TODO - try to clean this stuff up
const TodayDetails = ({
  data: {
    city,
    description,
    dewPoint,
    heatIndex,
    humidity,
    icon,
    pressure,
    temperature,
    wind,
    visibility,
  },
}) => (
  <DetailsSection>
    <TopRow>
      <div>
        {city}
        <span className={"description"}>
          <img
            alt={"weather icon"}
            className={"icon"}
            height={"32px"}
            src={require(`../assets/icons/${icon}`)}
            width={"32px"}
          />
          {description}
        </span>
      </div>
    </TopRow>

    <SecondRow>
      <div className={"temp-div"}>
        <span>
          {temperature}
          &deg;
        </span>
        <p className={"feels-like"}>
          {heatIndex}
          &deg;
        </p>{" "}
        {/* TODO - implement */}
      </div>

      <div className={"details"}>
        <div className={"col col-1"}>
          <div>
            <p>
              <b>Wind</b>
            </p>
            <p>{wind}</p>
          </div>
          <div>
            <p>
              <b>Humidity</b>
            </p>
            <p>{humidity}</p>
          </div>
        </div>
        <div className={"col col-2"}>
          <div>
            <p>
              <b>Dew Point</b>
            </p>
            <p>{dewPoint}</p>
          </div>
          <div>
            <p>
              <b>Pressure</b>
            </p>
            <p>{pressure}</p>
          </div>
        </div>
        <div className={"col col-3"}>
          <div>
            <p>
              <b>Visibility</b>
            </p>
            <p>{visibility}</p>
          </div>
        </div>
      </div>
    </SecondRow>
  </DetailsSection>
);

TodayDetails.propTypes = {
  data: PropTypes.shape({
    city: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    dewPoint: PropTypes.number,
    heatIndex: PropTypes.string,
    humidity: PropTypes.string,
    icon: PropTypes.string,
    pressure: PropTypes.number.isRequired,
    temperature: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
    visibility: PropTypes.string.isRequired,
  }),
};

TodayDetails.defaultProps = {
  data: {
    city: "Memphis",
    description: "Clear",
    dewPoint: 72,
    heatIndex: "74",
    humidity: "67%",
    icon: "",
    pressure: 901,
    temperature: 82,
    wind: "W 12mph",
    visibility: "2.87 mi",
  },
};

export default TodayDetails;
