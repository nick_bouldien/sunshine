import React from "react";
import styled from "styled-components";
import { Link } from "@reach/router";

// FIXME - this is incredibly ugly...
const SideBarNav = styled.nav`
  height: 1000px;
  z-index: 200;
  position: fixed;
  background-color: #48aafd;
  box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
  top: 30px;
  left: 0;
  min-width: 160px;
  width: 30%;

  ${({ show }) =>
    show
      ? `
      .side-bar {
        height: 100%;
        box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
        position: fixed;
        top: 0;
        left: 0;
        width: 70%;
        max-width: 400px;
        z-index: 200;
        transform: translateX(-100%);
        transition: transform 0.3s ease-out;      
      }

      a {
        color: #521751;
        text-decoration: none;
        font-size: 1rem;
      }
    
      ul {
        list-style: none;
        display: flex;
        flex-direction: column;
        justify-content: center;
      }
    
      li {
        margin: 0.5rem 0;
      }
    
      &.open {
        transform: translateX(0);
      }
  `
      : "display: none;"} @media (min-width: 601px) {
    display: none;
  }
`;

const SideBar = ({ show = true }) => {
  return (
    <SideBarNav className={"side-bar"} show={show}>
      <ul>
        <li>
          <Link to="/">Section 1</Link>
        </li>
        <li>
          <Link to="/">Section 2</Link>
        </li>
        <li>
          <Link to="/">Section 3</Link>
        </li>
      </ul>
    </SideBarNav>
  );
};

export default SideBar;
