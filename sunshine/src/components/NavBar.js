import React, { Component } from "react";
import { Link } from "@reach/router";
import NavBarToggleButton from "./NavBarToggleButton";
import Nav from "../styled_components/Nav";

class NavBar extends Component {
  render() {
    return (
      <Nav>
        <NavBarToggleButton click={this.props.click} />
        <Link to="/" className={"main"}>
          WeatherNow
        </Link>
        <div className={"navigation-links"}>
          <ul>
            <li>
              <Link to="/">Section 1</Link>
            </li>
            <li>
              <Link to="/">Section 2</Link>
            </li>
            <li>
              <Link to="/">Section 3</Link>
            </li>
          </ul>
        </div>
      </Nav>
    );
  }
}

export default NavBar;
