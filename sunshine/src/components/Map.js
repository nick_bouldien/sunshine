import React, { Component } from "react";
import * as PropTypes from "prop-types";
import Spinner from "./Spinner";
const MAP_KEY = process.env.REACT_APP_MAP_KEY || "";

class Map extends Component {
  state = {
    height: 525,
    locationSearch: "",
    width: 498,
    zoom: 13,
  };
  componentDidMount() {
    const { city, lat, lng, useLocation } = this.props;
    if (useLocation) {
      if (!lat || !lng) {
        return;
      } else {
        this.setState(prevState => ({
          ...prevState,
          locationSearch: `${lat},${lng}`,
        }));
      }
    } else {
      this.setState(prevState => ({
        ...prevState,
        locationSearch: city,
      }));
    }
  }
  componentDidUpdate(prevProps) {
    const { lat, lng, useLocation } = this.props;
    if (useLocation !== prevProps.useLocation || lat !== prevProps.lat || lng !== prevProps.lng) {
      this.setState(prevState => ({
        ...prevState,
        locationSearch: `${lat},${lng}`,
      }));
    }
  }
  render() {
    const { locationSearch, height, width, zoom } = this.state;
    const url = `https://maps.googleapis.com/maps/api/staticmap?center=${locationSearch}&zoom=${zoom}&size=${height}x${width}&key=${MAP_KEY}`;
    if (!locationSearch) {
      return <Spinner />;
    }
    return (
      <div>
        <img alt={"location map"} src={url} style={{ margin: "16px" }} />
      </div>
    );
  }
}

Map.propTypes = {
  city: PropTypes.string,
  lat: PropTypes.number,
  lng: PropTypes.number,
  useLocation: PropTypes.bool.isRequired,
};

export default Map;
