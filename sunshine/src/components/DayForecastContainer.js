import React from "react";
import styled from "styled-components";
import DayCard from "./DayCard";

const DayForecastList = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
`;

const DayForecastContainer = ({ days, selectedDay, selectDay }) => (
  <DayForecastList>
    {days
      ? days.map((day, ind) => (
          <DayCard
            day={day}
            index={ind}
            key={ind}
            isSelected={ind === selectedDay}
            selectDay={selectDay}
          />
        ))
      : null}
  </DayForecastList>
);

export default DayForecastContainer;
