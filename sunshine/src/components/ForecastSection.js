import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import PropTypes from "prop-types";
import styled from "styled-components";
import DayForecastContainer from "./DayForecastContainer";
import ErrorBoundary from "./ErrorBoundary";
import ForecastDetails from "./ForecastDetails";
import Spinner from "./Spinner";
import { createForecastUrl } from "../utils/urlCreator";

const H3 = styled.h3`
  color: ${props => props.theme.coolBlue};
  font-family: Open Sans Bold;
  font-size: 22pt;
  margin-bottom: 16px;
`;

@inject("forecastStore")
@observer
class ForecastSection extends Component {
  static propTypes = {
    city: PropTypes.string,
    count: PropTypes.number,
    lat: PropTypes.number,
    lng: PropTypes.number,
    useLocation: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    city: "atlanta",
    count: 5,
  };

  state = {
    selectedDay: 0,
  };

  componentDidMount() {
    if (this.props.useLocation && (!this.props.lat || !this.props.lng)) {
      // if using location, wait for the coordinates or an error to display
    } else {
      this.fetchData();
    }
  }

  componentDidUpdate(prevProps) {
    const { lat, lng } = this.props;
    if (
      this.props.useLocation !== prevProps.useLocation ||
      lat !== prevProps.lat ||
      lng !== prevProps.lng
    ) {
      this.setState(state => ({
        ...state,
        loading: true,
      }));
      this.fetchData();
    }
  }

  fetchData = () => {
    const { city, count, lat, lng, useLocation } = this.props;
    let url = "";
    if (useLocation && lat && lng) {
      url = createForecastUrl(null, count, lat, lng);
    } else {
      url = createForecastUrl(city, count);
    }
    this.props.forecastStore.fetchWeatherForecast(url);
  };

  selectDay = day => {
    this.setState(state => ({
      ...state,
      selectedDay: day,
    }));
  };

  selectedDayDetails = () => {
    const { weatherEntries } = this.props.forecastStore;
    const { selectedDay } = this.state;
    return weatherEntries[selectedDay];
  };

  render() {
    const { error, isLoading, weatherEntries } = this.props.forecastStore;
    const { selectedDay } = this.state;
    if (error) {
      return <code>error: {error.toString()}</code>;
    }
    if (isLoading) {
      return <Spinner />;
    }
    return weatherEntries ? (
      <ErrorBoundary>
        <div>
          <H3>{weatherEntries.length || 5} Day Forecast</H3>
          <DayForecastContainer
            days={weatherEntries}
            selectDay={this.selectDay}
            selectedDay={selectedDay}
          />
          <ForecastDetails dayData={this.selectedDayDetails()} />
        </div>
      </ErrorBoundary>
    ) : null;
  }
}

export default ForecastSection;
