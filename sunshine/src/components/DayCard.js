import React from "react";
import DayList from "../styled_components/DayList";
import { iconEnum } from "../utils/iconFinder";

const DayCard = ({ index, selectDay, isSelected, day: { dayName, highTemp, icon, lowTemp } }) => {
  const iconPath = iconEnum(icon);
  const image = require(`../assets/icons/${iconPath}`);
  return (
    <DayList
      className={isSelected ? "highlight" : ""}
      isSelected={isSelected}
      onClick={() => selectDay(index)}
    >
      <p className={"day-title"}>{dayName}</p>
      <img src={image} alt={"weather icon"} />
      <div className={"temp-range"}>
        <span>
          {highTemp}
          &deg;
        </span>
        <span>
          {lowTemp}
          &deg;
        </span>
      </div>
    </DayList>
  );
};

export default DayCard;
