import React from "react";
import styled from "styled-components";
import Map from "./Map";

const RadarDiv = styled.div`
  border: 1px solid #003459;
  max-width: 600px;

  h3 {
    color: #003459;
    font-family: Open Sans Bold;
    font-size: 20px;
    margin-bottom: 0;
    margin-left: 16px;
  }
`;

const RadarSection = props => (
  <RadarDiv>
    <h3>Map</h3>
    <Map {...props} />
  </RadarDiv>
);

export default RadarSection;
