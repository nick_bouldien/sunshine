import React from "react";
import DetailsContainer from "../styled_components/DetailsContainer";

const ForecastDetails = ({ dayData }) => {
  // TODO: - need to get UV, sunrise, and sunset data
  return (
    <DetailsContainer>
      <div>
        <p className={"bold"}>Wind</p>
        <p>{dayData.wind}</p>
      </div>
      <div>
        <p className={"bold"}>Humidity</p>
        <p>{dayData.humidity}</p>
      </div>
      <div>
        <p className={"bold"}>UV Index</p>
        <p>3 of 10</p>
      </div>
      <div>
        <p className={"bold"}>Sunrise</p>
        <p>6:49 am</p>
      </div>
      <div>
        <p className={"bold"}>Sunset</p>
        <p>7:47 pm</p>
      </div>
    </DetailsContainer>
  );
};

export default ForecastDetails;
