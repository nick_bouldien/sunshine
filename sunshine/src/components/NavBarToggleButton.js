import React from "react";
import menuSvg from "../assets/icons/menu.svg";

const NavBarToggleButton = ({ click }) => (
  <div className={"toggle-button"} onClick={click}>
    <img src={menuSvg} alt={"menu"} height={"20px"} width={"20px"} />
  </div>
);

export default NavBarToggleButton;
