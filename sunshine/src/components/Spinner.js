import React from "react";
import styled, { keyframes } from "styled-components";
import spinnerImage from "../assets/images/loading.png";

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Image = styled.img`
  animation: ${spin} 4s infinite linear;
  display: block;
  height: auto;
  margin-bottom: 10px;
  width: 10%;
`;

const Spinner = () => <Image src={spinnerImage} alt="loading indicator" />;

export default Spinner;
