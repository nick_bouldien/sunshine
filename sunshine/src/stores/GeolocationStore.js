import { types } from "mobx-state-tree";

const Coordinates = types.model("Coordinates", {
  lat: types.number,
  lng: types.number,
});

const GeolocationStore = types
  .model("GeolocationStore", {
    coordinates: types.map(Coordinates),
    error: types.optional(types.string),
    useLocation: types.boolean,
  })
  .views(self => ({
    get useLocation() {
      return self.useLocation;
    },
  }))
  .actions(self => {
    function toggleUseLocation() {
      self.useLocation = !self.useLocation;
    }

    return {
      toggleUseLocation,
    };
  });
