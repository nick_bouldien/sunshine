import { types, flow } from "mobx-state-tree";
import { getDayString } from "../utils/dates";
import { degToCompass } from "../utils/direction";

const WeatherEntry = types.model("WeatherEntry", {
  dayName: types.string,
  highTemp: types.string,
  humidity: types.string,
  icon: types.optional(types.string, ""),
  lowTemp: types.string,
  pressure: types.number,
  wind: types.string,
});

export const ForecastStore = types
  .model("ForecastStore", {
    error: types.optional(types.string, ""),
    isLoading: types.boolean,
    selectedDay: types.number,
    weatherEntries: types.array(WeatherEntry),
  })
  .views(self => ({
    get count() {
      return self.weatherEntries.length;
    },
  }))
  .actions(self => {
    function markLoading(loading) {
      self.isLoading = loading;
    }
    function updateWeatherForecast(json) {
      const formatted = formatData(json);
      formatted.forEach(weatherJson => {
        self.weatherEntries.push(weatherJson);
      });
    }

    const fetchWeatherForecast = flow(function* fetchWeatherForecast(url) {
      try {
        const res = yield fetch(url);
        if (res.status !== 200) {
          const msg = `Looks like there was a problem. Status Code: ${res.status}`;
          markLoading(false);
          self.error = msg;
          return;
        }
        const json = yield res.json();
        updateWeatherForecast(json);
        markLoading(false);
      } catch (err) {
        console.error("Failed to fetch weather ", err);
        const msg = `Failed to fetch the weather: ${err}`;
        markLoading(false);
        self.error = msg;
      }
    });

    return {
      fetchWeatherForecast,
    };
  });

const formatData = ({ list }) => {
  // TODO - need more falsy checking
  return (
    list &&
    list.map(day => ({
      dayName: getDayString(day.dt * 1000),
      highTemp: day.temp.max.toFixed(),
      humidity: `${day.humidity}%`,
      icon: day.weather[0].icon,
      lowTemp: day.temp.min.toFixed(),
      pressure: day.pressure,
      wind: `${degToCompass(day.deg)} ${day.speed.toFixed()}mph`, // hardcoded units...
    }))
  );
};

const forecastStore = ForecastStore.create({
  error: "",
  isLoading: true,
  selectedDay: 0,
  weatherEntries: [],
});

export default forecastStore;
