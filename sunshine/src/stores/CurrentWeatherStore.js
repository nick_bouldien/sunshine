import { types, flow } from "mobx-state-tree";
import { degToCompass } from "../utils/direction";
import { calculateDewPoint, calculateHeatIndex } from "../utils/temperature_utils";
import { iconEnum } from "../utils/iconFinder";

const CurrentWeatherEntry = types.model("CurrentWeatherEntry", {
  city: types.maybe(types.string),
  description: types.maybe(types.string),
  dewPoint: types.maybe(types.number),
  heatIndex: types.maybe(types.string),
  humidity: types.maybe(types.string),
  icon: types.maybe(types.string),
  pressure: types.maybe(types.number),
  temperature: types.maybe(types.number),
  wind: types.maybe(types.string),
  visibility: types.maybe(types.string),
});

export const CurrentWeatherStore = types
  .model("CurrentWeatherStore", {
    error: types.optional(types.string, ""),
    isLoading: types.boolean,
    weather: types.maybeNull(CurrentWeatherEntry),
  })
  .views(self => ({
    //
  }))
  .actions(self => {
    function markLoading(loading) {
      self.isLoading = loading;
    }
    function updateCurrentWeather(json) {
      self.weather = formatWeatherData(json);
    }

    const fetchCurrentWeather = flow(function* fetchCurrentWeather(url) {
      try {
        const res = yield fetch(url);
        if (res.status !== 200) {
          const msg = `Looks like there was a problem. Status Code: ${res.status}`;
          markLoading(false);
          self.error = msg;
          return;
        }
        const json = yield res.json();
        updateCurrentWeather(json);
        markLoading(false);
      } catch (err) {
        console.error("Failed to fetch weather ", err);
        const msg = `Failed to fetch the weather: ${err}`;
        markLoading(false);
        self.error = msg;
      }
    });

    return {
      fetchCurrentWeather,
    };
  });

const formatWeatherData = data => {
  // being aggressive aqui - should probably do more falsy checks
  const { humidity, pressure, temp } = data.main;
  const visInMiles = (data.visibility / 5280).toFixed(1); // would need to check on units (use miles/km/etc.)
  const { deg, speed } = data.wind;
  const iconKey = data.weather[0].icon;

  return {
    city: data.name,
    description: data.weather[0].main,
    dewPoint: calculateDewPoint(temp, humidity),
    heatIndex: `Feels like ${calculateHeatIndex(temp, humidity)}`,
    humidity: `${humidity}%`,
    icon: iconEnum(iconKey),
    pressure,
    temperature: Math.round(temp),
    wind: `${degToCompass(deg)} ${speed}mph`,
    visibility: `${visInMiles} mi`,
  };
};

const currentWeatherStore = CurrentWeatherStore.create({
  error: "",
  isLoading: true,
  weather: null,
});

export default currentWeatherStore;
