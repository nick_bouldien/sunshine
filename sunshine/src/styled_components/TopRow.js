import styled from "styled-components";

const TopRow = styled.div`
  margin-bottom: 16px;
  .icon {
    fill: white;
  }
  div {
    font-size: 26pt;
    .description {
      font-size: 14pt;
      margin-left: 32px;
    }
  }
`;

export default TopRow;
