import styled from "styled-components";

const DetailsContainer = styled.div`
  align-items: center;
  background-color: ${props => props.theme.coolBlue};
  color: white;
  display: flex;
  height: 121px;
  justify-content: space-around;
  padding-left: 32px;
  padding-right: 100px;

  .bold {
    font-weight: bold;
  }

  p {
    margin: 8px;
  }
`;
export default DetailsContainer;
