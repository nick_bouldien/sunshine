import styled from "styled-components";

const MainContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding-left: 16px;
  padding-right: 16px;

  .left-col {
    margin-bottom: 16px;
    margin-right: 16px;
    width: 700px;
  }
`;

export default MainContainer;
