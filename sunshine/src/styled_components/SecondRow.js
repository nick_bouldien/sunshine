import styled from "styled-components";

const SecondRow = styled.div`
  display: flex;
  align-content: flex-start;

  span {
    font-size: 56pt;
  }

  .col {
    margin-right: 32px;
  }

  .feels-like {
    margin-top: 4px;
  }

  .details {
    display: flex;

    p {
      margin-top: 8px;
      margin-bottom: 8px;
    }
  }

  .temp-div {
    margin-right: 32px;
  }
`;

export default SecondRow;
