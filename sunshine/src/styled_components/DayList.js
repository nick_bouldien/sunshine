import styled from "styled-components";

const DayList = styled.div`
  border: 1px solid ${props => props.theme.coolBlue};
  color: ${props => props.theme.coolBlue};
  font-family: Open Sans Regular;
  font-size: 12pt;
  height: ${props => (props.isSelected ? "129px" : "121px")}; // these could go in .highlight...
  margin-bottom: ${props => (props.isSelected ? "0px" : "8px")};
  text-align: center;
  width: 128px;

  &:hover {
    background-color: #d4d4d4;
    cursor: pointer;
  }

  .day-title {
    font-family: Open Sans Bold;
    font-size: 12pt;
    margin-top: 16px;
    margin-bottom: 8px;
  }

  &.highlight {
    background-color: ${props => props.theme.coolBlue};
    color: #ffffff;
  }

  img {
    height: 32px;
    width: 32px;
  }

  .temp-range {
    display: flex;
    justify-content: space-around;
  }
`;

export default DayList;
