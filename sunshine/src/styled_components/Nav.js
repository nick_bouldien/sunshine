import styled from "styled-components";

const Nav = styled.nav`
  height: 65px;
  display: flex;
  align-items: center;
  padding-left: 16px;

  a {
    color: ${props => props.theme.coolBlue};
    font-family: Open Sans Regular;
    font-size: 14pt;
    margin-right: 2rem;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }

    &.main {
      font-family: Open Sans Extra Bold;
      font-size: 18pt;
    }
  }

  .navigation-links ul {
    display: flex;
    list-style: none;
    margin: 0;
    padding: 0;
  }

  @media (max-width: 600px) {
    // background: ${props => props.theme.coolBlue};
    height: 30px;
    margin-bottom: 8px;

    a.main {
      margin-left: 1rem;
      font-size: 16pt;
    }

    .navigation-links {
      display: none;
    }
  }

  @media (min-width: 601px) {
    background: #fff;

    .toggle-button {
      display: none;
    }
  }
`;

export default Nav;
