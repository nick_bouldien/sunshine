import React, { Component } from "react";
import ForecastSection from "../components/ForecastSection";
import GeoContainer from "../components/GeoContainer";
import TodayDetailsContainer from "../components/TodayDetailsContainer";
import RadarSection from "../components/RadarSection";
import MainContainer from "../styled_components/MainContainer";

class Home extends Component {
  state = {
    city: "memphis", // will be able to be changed by some input
    count: 5,
    useLocation: true,
  };

  toggleUseLocation = () => {
    this.setState(state => ({
      ...state,
      useLocation: !state.useLocation,
    }));
  };

  render() {
    const { city, count, useLocation } = this.state;
    return (
      <GeoContainer>
        {({ coordinates, error }) =>
          // instead of showing error if user doesn't allow geolocation, just use the default city
          // until they change to their desired city (TODO)
          useLocation && error ? (
            <>
              <p>
                <code>Error: {JSON.stringify(error, null, 4)}</code>
              </p>
              <button onClick={this.toggleUseLocation}>
                {useLocation && "Don't"} use your location
              </button>
            </>
          ) : (
            <MainContainer>
              <div className={"left-col"}>
                {/* TODO- use context/mobx to pass down props aqui */}
                <TodayDetailsContainer
                  city={city}
                  lat={coordinates.lat}
                  lng={coordinates.lng}
                  count={count}
                  useLocation={useLocation}
                />
                <ForecastSection
                  city={city}
                  lat={coordinates.lat}
                  lng={coordinates.lng}
                  count={count}
                  useLocation={useLocation}
                />
              </div>
              <RadarSection
                city={city}
                lat={coordinates.lat}
                lng={coordinates.lng}
                count={count}
                useLocation={useLocation}
              />
            </MainContainer>
          )
        }
      </GeoContainer>
    );
  }
}

export default Home;
