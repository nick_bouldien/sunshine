export const getDayString = timestamp =>
  new Date(timestamp).toLocaleString("en-us", { weekday: "long" });
