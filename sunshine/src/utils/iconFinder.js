const icons = {
  "11d": "weather-stormy.svg",
  "09d": "weather-rainy.svg",
  "10d": "weather-rainy-2.svg",
  "13d": "weather-rainy-2.svg",
  "50d": "weather-sunny.svg",
  "01d": "weather-sunny.svg",
  "01n": "weather-moon.svg",
  "02d": "weather-cloudy.svg",
  "02n": "weather-cloudy-night.svg",
};

export const iconEnum = iconId => icons[iconId] || "weather-stormy.svg"; // using stormy as default for now
