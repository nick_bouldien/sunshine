// https://www.wpc.ncep.noaa.gov/html/heatindex_equation.shtml
export const calculateHeatIndex = (temp = 60, humidity = 0.5) => {
  // FIXME - make sure this is correct - right now somewhat off...
  let heatIndex =
    -42.379 +
    2.04901523 * temp +
    10.14333127 * humidity -
    0.22475541 * temp * humidity -
    0.00683783 * temp * temp -
    0.05481717 * humidity * humidity +
    0.00122874 * temp * temp * humidity +
    0.00085282 * temp * humidity * humidity -
    0.00000199 * temp * temp * humidity * humidity;

  // if (humidity < 0.13 && (temp < 112 && temp > 80)) {
  //   // ADJUSTMENT = [(13-RH)/4]*SQRT{[17-ABS(T-95.)]/17}
  //   const adjustment = ((13 - humidity) / 4) * Math.sqrt((17 - Math.abs(temp - 95)) / 17);
  // }

  // HI = 0.5 * {T + 61.0 + [(T-68.0)*1.2] + (RH*0.094)}
  const basic = 0.5 * (temp + 61 + (temp - 68) * 1.2 + humidity * 0.094);

  return Math.round(basic);
};

// FIXME
export const calculateDewPoint = (temp = 60, humidity = 0.5) => {
  //  https://www.ajdesigner.com/phphumidity/dewpoint_equation_dewpoint_temperature.php
  // TODO - need flag for C/F
  // const t = (temp - 32) * (5 / 9);
  const s = (humidity / 100) ^ (1 / 8);

  const dp = s * (112 + 0.9 * temp) + 0.1 * temp - 112;

  return Math.round(dp);
};
