const API_KEY = process.env.REACT_APP_API_KEY || "";
console.info("API_KEY: ", API_KEY);

// one day: `http://api.openweathermap.org/data/2.5/weather?q=${city}&mode=json&units=imperial&apikey=${API_KEY}`;
export const createCurrentWeatherUrl = (city = "memphis", lat = 35.14, long = 90.04) => {
  let url = "";
  if (city) {
    url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&mode=json&units=imperial&apikey=${API_KEY}`;
  } else {
    url = `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&mode=json&units=imperial&apikey=${API_KEY}`;
  }
  console.log("current weather url: ", url);

  return url;
};

// 5 day forecast:  `http://api.openweathermap.org/data/2.5/forecast/daily?q=${city}&mode=json&cnt=${count}&units=imperial&apikey=${API_KEY}`;
export const createForecastUrl = (city, count = 5, lat = 35.14, long = 90.04) => {
  // sunrise/sunset data not available in json format - only xml...
  // https://openweathermap.desk.com/customer/portal/questions/16990417-sun-rise-set-in-forecast?t=535697
  let url = "";
  if (city) {
    url = `http://api.openweathermap.org/data/2.5/forecast/daily?q=${city}&mode=json&cnt=${count}&units=imperial&apikey=${API_KEY}`;
  } else {
    url = `http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${long}&mode=json&cnt=${count}&units=imperial&apikey=${API_KEY}`;
  }
  console.log("forecast url: ", url);

  return url;
};

// http://api.openweathermap.org/data/2.5/uvi/forecast?appid={appid}&lat={lat}&lon={lon}&cnt={cnt}
// export const createUVIndexUrl = (lat = 35.14, long = 90.04) => {
//   const url = `http://api.openweathermap.org/data/2.5/uvi?lat=${lat}&lon=${long}&mode=json&units=imperial&apikey=${API_KEY}`;
//   console.log("UV url: ", url);
//
//   return url;
// };

// https://openweathermap.org/api/weather-map-2
// http://maps.openweathermap.org/maps/2.0/weather/{op}/{z}/{x}/{y}
// export const createMapUrl = (
//   layer = "temp_new",
//   x = 2,
//   y = 2,
//   z = 10,
//   // op = "TA2",
//   // opacity = 1,
//   // date,
//   // fill_bound = false,
// ) => {
//   const url = `https://tile.openweathermap.org/map/temp_new/${z}/${x}/${y}.png?appid=${API_KEY}`;
//   // const url = `https://maps.owm.io/map/temp_new/${z}/${x}/${y}.png?appid=${API_KEY}`; // non free version
//
//   // https://maps.owm.io/map/{layer}/{z}/{x}/{y}.png?appid={api_key}
//   console.log("map url: ", url);
//   return url;
// };
