import React, { Component } from "react";
import { Router } from "@reach/router";
import { injectGlobal, ThemeProvider } from "styled-components";
import { Provider } from "mobx-react";
import About from "./pages/About";
import Home from "./pages/Home";
import NavBar from "./components/NavBar";
import forecastStore from "./stores/ForecastStore";
import currentWeatherStore from "./stores/CurrentWeatherStore";
import Backdrop from "./components/Backdrop";
import SideBar from "./components/Sidebar";

const theme = {
  black: "#393939",
  coolBlue: "#003459",
  maxWidth: "1200px",
  margin: "0 auto",
};

injectGlobal`
  html {
    box-sizing: border-box;
    font-size: 16px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    margin: 0;
    padding: 0;
  }
`;

const MainContainer = props => <div className={"App"}>{props.children}</div>;

class App extends Component {
  state = {
    sidebarOpen: false,
  };

  drawerToggleClickHandler = () => {
    this.setState(state => ({
      ...state,
      sidebarOpen: !state.sidebarOpen,
    }));
  };

  backdropClickHandler = () => {
    this.setState({ sidebarOpen: false });
  };

  render() {
    const { sidebarOpen } = this.state;
    let backdrop = null;
    if (sidebarOpen) {
      backdrop = <Backdrop click={this.backdropClickHandler} />;
    }
    return (
      <Provider forecastStore={forecastStore} currentWeatherStore={currentWeatherStore}>
        <ThemeProvider theme={theme}>
          <MainContainer>
            <NavBar click={this.drawerToggleClickHandler} />
            <SideBar show={sidebarOpen} />
            {backdrop}
            <Router>
              <Home path="/" />
              <About path="about" />
            </Router>
          </MainContainer>
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
