"use strict";

const Hapi = require("hapi");
const inert = require("inert");
const path = require("path");

const port = process.env.API_PORT || 3001;

const server = Hapi.server({
  port,
  host: "localhost",
  routes: {
    cors: {
      origin: ["*"],
    },
    files: {
      relativeTo: path.join(__dirname, "public"),
    },
  },
});

const init = async () => {
  await server.register(inert);

  server.route({
    method: "GET",
    path: "/{path*}",
    handler: {
      // file: path.join(__dirname, "build", "index.html"),
      directory: {
        path: path.join(__dirname, "build"),
        redirectToSlash: true,
        index: true,
      },
    },
  });

  await server.start();

  console.info(`Server running at: ${server.info.uri}`);
};

process.on("unhandledRejection", err => {
  console.error(err);
  process.exit(1);
});

init();

// source - https://github.com/notrab/create-react-app-hapi/blob/master/server.js
