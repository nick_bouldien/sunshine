# WeatherNow

Project to show the 5 day weather forecast and current weather for a given city or geolocation. (bootstrapped with create-react-app (ejected).
Playground to mess around with mobx-state-tree and styled-components (and add a super simple Hapi server)

### Install / Start Dev
- clone repo
- change to project directory
- yarn install
- add your open weather map api key to .env.local
- yarn start

#### Resources:
- [Open Weather Map API](https://openweathermap.org/api)
- [Repo by Jamie Barton (hapi)](https://github.com/notrab/create-react-app-hapi)
- [mobx-state-tree repo](https://github.com/mobxjs/mobx-state-tree)
- [hapi.js docs](https://hapijs.com/)
- [responsive nav bar](https://www.youtube.com/watch?v=l6nmysZKHFU)

#### TODOs:
- ~~add a state management library (just for fun)~~
- ~~use geolocation~~
- ~~add (basic) server~~
- ~~get wind direction from degrees~~
- ~~move "style-only" components to new dir~~
- ~~make spinner component~~
- let user enter in city or zip code
- get UV index (https://openweathermap.org/api/uvi)
- styling/responsiveness/grid!!! (tablet/mobile)
- get sunrise/sunset (not in json response, only in xml...)
- more error handling (error boundaries?)
- add accessibility
- dynamic/"real" map
- add tests
- favicon
- make icons white

#### WIP:
- "feels like" temp
- dew point
